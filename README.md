# Singleton SD - Puppeteer

Contains necessary files to create a docker image with puppeteer.

## Documentation

https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#running-puppeteer-in-docker

## Dockerfile

https://github.com/GoogleChrome/puppeteer/blob/master/.ci/node8/Dockerfile.linux

----------------------

© [Singleton](http://singletonsd.com), Italy, 2019.
